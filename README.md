# Projet de webscrapping : analyse d'un cours de la bourse :moneybag: :chart_with_upwards_trend:

<p>:mortar_board: <strong>Ecole d'ingénieur:</strong> Etudiante en Master 2 à l'ESME Sudria en majeure Intelligence artificielle </p>
<p>:computer: <strong>But du projet :</strong> Réalisation d'une application permettant de voir l'évolution d'un cours de la bourse et de l'analyser. Le but principal du projet n'est pas la réalisation du projet à proprement parler, mais l'apprentissage du webscrapping, du machine learning et des librairies comme pandas, numpy, sklearn, tkinter ...</p>
<p>:iphone: <strong>Contact:</strong> marie.mazoyer@esme.fr  et linkedin : <a href="https://www.linkedin.com/in/marie-mazoyer-398211124/">mariemazoyerlinkedin.fr</a></p>
